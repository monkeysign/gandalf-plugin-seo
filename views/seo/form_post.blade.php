<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h2>Pannello SEO</h2>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label title="Campo obbligatorio"
                               class="control-label">Titolo</label>
                        <div class="input-group">
                            <input value="{{$record['title']}}"
                                   data-toggle="validator" type="text" name="seo[title]"
                                   id="seo_title" class="form-control"
                            >
                        </div>
                        <span class="help-block with-errors"> </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label title="Campo obbligatorio"
                               class="control-label">Descrizione</label>
                        <div class="input-group">
                            <textarea name="seo[description]" class="form-control">{{$record['description']}}</textarea>
                        </div>
                        <span class="help-block with-errors"> </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>