@if($el)
    <title>@if($seo['title'] !== ''){{$seo['title']}}@else{{$el->meta('title') . ' - ' . config('appname') }}@endif</title>
    <meta name="description"
          content="@if($seo['description'] !== ''){{$seo['description']}}@else {{config('appdescription')}}@endif">
@else
    <title>{{ config('appname') }}</title>
    <meta name="description"
          content="{{ config('appdescription')}}">
@endif