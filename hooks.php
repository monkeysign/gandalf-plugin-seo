<?php
/**
 * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
 */
hooks()->add_filter( APP_VIEWS_PATH, function ( $views = [] ) {
	return array_merge( [ __DIR__ . '/views/' ], $views );
} );

/**
 * Aggiungo le voci al menu tramite una vista presente nel plugin
 */
hooks()->add_action( ADMIN_LEFT_MENU, function () {
	view()->render( 'seo.left', [] );
} );


/*
 * ----------------------------------
 * ACTION PER AGGIUNGERE AL PLUGIN CMS
 * -----------------------------------
 */
/**
 * Aggiungo i campi SEO al form post tramite l'utilizzo dell'action
 */
hooks()->add_action( CMS_ADMIN_POST_FORM_LEFT, function ( $post ) {
	$record = unserialize( $post->meta( 'seo' ) );
	view()->render( 'seo.form_post', [ 'record' => $record ] );
} );

/**
 * Aggiungo l'inserimento dei campi al salvataggio del post
 */
hooks()->add_action( CMS_ADMIN_POST_SAVE, function ( $post ) {
	$seo = request()->get( 'seo' );
	// cancello il vecchio seo
	\Plugins\CMS\Models\PostMeta::where( 'id_post', '=', $post->id )->where( 'iso', config( 'locale' ) )->where( 'meta_key', 'seo' )->delete();
	// aggiungo il nuovo
	$metaArray['meta_key'] = 'seo';
	$metaArray['value']    = serialize( $seo );
	$metaArray['id_post']  = $post->id;
	$metaArray['iso']      = config( 'locale' );
	\Plugins\CMS\Models\PostMeta::create( $metaArray );
} );

/**
 * Aggiungo i campi SEO al form delle categorie tramite l'utilizzo dell'action
 */
hooks()->add_action( CMS_ADMIN_POST_CATEGORY_FORM, function ( $post ) {
	$record = unserialize( $post->meta( 'seo' ) );
	view()->render( 'seo.form_post', [ 'record' => $record ] );
} );

/**
 * Aggiungo l'inserimento dei campi al salvataggio del post
 */
hooks()->add_action( CMS_ADMIN_POST_CATEGORY_SAVE, function ( $post ) {
	$seo = request()->get( 'seo' );
	// cancello il vecchio seo
	\Plugins\CMS\Models\CatPostTypeMeta::where( 'id_category_post_type', '=', $post->id )->where( 'iso', config( 'locale' ) )->where( 'meta_key', 'seo' )->delete();
	// aggiungo il nuovo
	$metaArray['meta_key']              = 'seo';
	$metaArray['value']                 = serialize( $seo );
	$metaArray['id_category_post_type'] = $post->id;
	$metaArray['iso']                   = config( 'locale' );
	\Plugins\CMS\Models\CatPostTypeMeta::create( $metaArray );
} );


/*
 * -----------------------------------
 * FRONTEND
 * -----------------------------------
 */
hooks()->add_action( THEME_HEADER_SEO, function ( $arg ) {
	// nel caso di un post o categoria del plugin CMS
	if ( isset( $arg['post'] ) ) {
		$el = $arg['post'];
	} // nel caso di un post o categoria del plugin Ecommerce
	elseif ( isset( $arg['product'] ) ) {
		$el = $arg['product'];
	} else {
		$el = $arg;
	}

	if ( is_object( $el ) ) {
		$seo = unserialize( $el->meta( 'seo' ) );
	} else {
		if ( isset( $el['seo'] ) ) {
			$seo = $el['seo'];
			$el  = true;
		} else {
			$el  = null;
			$seo = null;
		}
	}
	view()->render( 'seo.header', [ 'el' => $el, 'seo' => $seo ] );
} );

/**
 * Includo gli helper
 */
require __DIR__ . '/helpers.php';